import React from 'react';
import Toolbar from '@material-ui/core/Toolbar';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import IconButton from '@material-ui/core/IconButton';
import PersonAddIcon from '@material-ui/icons/PersonAdd';
import NotificationsNoneIcon from '@material-ui/icons/NotificationsNone';
import Button from '@material-ui/core/Button';
import { Divider } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import Switch from '@material-ui/core/Switch';
import Avatar from '@material-ui/core/Avatar';

export default function UserOptions() {
    const [anchorEl, setAnchorEl] = React.useState(null);
    const [checked, setchecked] = React.useState(false);
    const open = Boolean(anchorEl);
    const AntSwitch = withStyles(theme => ({
        root: {
            width: 28,
            height: 16,
            padding: 0,
            display: 'flex',
        },
        switchBase: {
            padding: 2,
            color: theme.palette.grey[500],
            '&$checked': {
                transform: 'translateX(12px)',
                color: theme.palette.common.white,
                '& + $track': {
                    opacity: 1,
                    backgroundColor: theme.palette.primary.main,
                    borderColor: theme.palette.primary.main,
                },
            },
        },
        thumb: {
            width: 12,
            height: 12,
            boxShadow: 'none',
        },
        track: {
            border: `1px solid ${theme.palette.grey[500]}`,
            borderRadius: 16 / 2,
            opacity: 1,
            backgroundColor: theme.palette.common.white,
        },
        checked: {},
    }))(Switch);

    const handleMenu = event => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const handleChange = () => {
        setchecked(!checked);
    }
    return (
        <Toolbar style={{'marginLeft' : 'auto'}}>
            <div >
                <Button
                    aria-label="invite team member"
                    color="inherit"
                >
                    <PersonAddIcon />INVITE TEAM MEMBER
                    </Button>
                <Button
                    aria-label="notification icon"
                    color="inherit"
                >
                    <NotificationsNoneIcon />
                </Button>
                <IconButton
                    aria-label="account of current user"
                    aria-controls="menu-appbar"
                    aria-haspopup="true"
                    onClick={handleMenu}
                    color="inherit"
                >
                    <Avatar>FL</Avatar>
                </IconButton>
                <Menu
                    id="menu-appbar"
                    anchorEl={anchorEl}
                    anchorOrigin={{
                        vertical: 'top',
                        horizontal: 'right',
                    }}
                    keepMounted
                    transformOrigin={{
                        vertical: 'top',
                        horizontal: 'right',
                    }}
                    open={open}
                    onClose={handleClose}
                >
                    <MenuItem >
                        Dark Mode
                        <AntSwitch checked={checked} onChange={handleChange} name="switch" />
                    </MenuItem>
                    <MenuItem onClick={handleClose}>Profile</MenuItem>
                    <Divider />
                    <MenuItem onClick={handleClose}>What's New</MenuItem>
                    <MenuItem onClick={handleClose}>Help</MenuItem>
                    <MenuItem onClick={handleClose}>Send Feedback</MenuItem>
                    <MenuItem onClick={handleClose}>Hints and shortcuts</MenuItem>
                    <Divider />
                    <MenuItem onClick={handleClose}>Log out</MenuItem>
                </Menu>
            </div>
        </Toolbar>
    )
}