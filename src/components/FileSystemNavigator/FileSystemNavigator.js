import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TreeView from '@material-ui/lab/TreeView';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import Container from '../Container/Container';
import Node from '../Node/Node';
import AddContainerPane from '../AddContainerPane/AddContainerPane';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Radio from '@material-ui/core/Radio';

const useStyles = makeStyles({
    root: {
      height: 240,
      flexGrow: 1,
      maxWidth: 400,
    },
  });

export default function FileSystemNavigator() {
    const classes = useStyles();
    const [openDialog, setOpenDialog] = useState();
    const [ selectedValue, setSelectedValue ] = useState();
    const [containerId, setContainerId] = useState();
    const [fileSystem, setFileSystem] = useState([
        {
            "label": "container.1",
            "type": "container",
            "isCollapsed": false,
            "id": "#1",
            "children": [{
                "label": "container.1.1",
                "type": "container",
                "isCollapsed": false,
                "id": "#1#1",
                "children": [{
                    "label": "container.1.1.1",
                    "type": "container",
                    "isCollapsed": false,
                    "id": "#1#1#1",
                    "children": []
                }]
            }],
        },
        {
            "label": "container.2",
            "type": "container",
            "isCollapsed": false,
            "id": "#2",
            "children": [{
                "label": "node.2.1",
                "type": "node",
                "id": "#2#1"
            }],
        }
        
    ]);

    const handlePaneClick = () => {
       
        setFileSystem([
            ...fileSystem,
            {
                "label": "container."+ (fileSystem.length+1),
                "type": "container",
                "isCollapsed": false,
                "id": "#" + (fileSystem.length+1),
                "children": [] 
            }
        ])
    }
    const getNewContainer = (containerLevels, parentJson) => {
        return {
            "label": "container."+ (containerLevels.join(".") + "." + (parentJson.children.length+1)),
            "type": "container",
            "isCollapsed": false,
            "id": "#" + (containerLevels.join("#") + "#" + (parentJson.children.length+1)),
            "children": [] 
        }
    }

    const getNewNode = (containerLevels, parentJson) => {
        return {
            "label": "node."+ (containerLevels.join(".") + "." + (parentJson.children.length+1)),
            "type": "node",
            "id": "#" + (containerLevels.join("#") + "#" + (parentJson.children.length+1)),
        }
    }

    const handleAddContainerClick = (containerId) => {
        setOpenDialog(true);
        setContainerId(containerId);
    }

    const pushContinerAtCorrectPlace = (nodeType, containerLevels, i, parentJson) => {
        if(i === containerLevels.length){
            if(nodeType === 'container'){
                var newContainer = getNewContainer(containerLevels, parentJson);
                parentJson.children.push(newContainer);
            } else {
                var newNode = getNewNode(containerLevels, parentJson);
                parentJson.children.push(newNode);
            }
            return;
        }
        pushContinerAtCorrectPlace(nodeType, containerLevels, i+1, parentJson.children[containerLevels[i]-1]);
    }
    const handleDialogClose = () => {
        setOpenDialog(false);
    };

    const handleRadioSelect = (event) => {
        setOpenDialog(false);
        var stringContainerLevels = containerId.split("#").slice(1, containerId.length);
        var containerLevels = stringContainerLevels.map(item => {
            return parseInt(item);
        })
        pushContinerAtCorrectPlace(event.target.value, containerLevels, 1, fileSystem[containerLevels[0]-1]);
        setFileSystem([...fileSystem]);
        setSelectedValue(undefined)
    }
    return(
        <div>
        <AddContainerPane onButtonClick={handlePaneClick}/>
        <TreeView
        className={classes.root}
        defaultCollapseIcon={<ExpandMoreIcon />}
        defaultExpandIcon={<ChevronRightIcon />}>
        {fileSystem.map(item => {
            if (item.type === 'container'){
                return(<Container onAddContainerClick={handleAddContainerClick} key={item.id} {...item}/>)
            } else {
                return(<Node key={item.id} {...item}/>)
            }
        })}
       
        </TreeView>
        <Dialog open={openDialog} onClose={handleDialogClose} aria-labelledby="form-dialog-title">
            <DialogTitle id="form-dialog-title">Add container or node?</DialogTitle>
            <DialogContent>
                Container <Radio
                    onChange={handleRadioSelect}
                    value="container"
                    name="radio-button-demo"
                    inputProps={{ 'aria-label': 'container' }}
                />
                Node <Radio
                    onChange={handleRadioSelect}
                    value="node"
                    name="radio-button-demo"
                    inputProps={{ 'aria-label': 'node' }}
                />
            </DialogContent>
        </Dialog>
        </div>
    );
}
