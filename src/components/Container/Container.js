import React, { useState } from 'react';
import TreeItem from '@material-ui/lab/TreeItem';
import Node from '../Node/Node';
import AddIcon from '@material-ui/icons/Add';
import Grid from '@material-ui/core/Grid';
import Tooltip from '@material-ui/core/Tooltip';

export default function Container(props) {
    const [containerState] = useState(props);

    return (
        <Grid
            container
            direction="row"
            justify="flex-end"
            alignItems="flex-start"
        >
            <Grid item xs>
                <TreeItem
                    nodeId={containerState.id}
                    label={containerState.label}
                   >
                    {containerState.children.map(child => {
                        if (child.type === 'container') {
                            return (<Container onAddContainerClick={props.onAddContainerClick} key={child.id} {...child} />)
                        } else {
                            return (<Node key={child.id} {...child} />)
                        }
                    })}

                </TreeItem></Grid>
            <Grid>
            <Tooltip title="Create Item" arrow placement="top"> 
            <AddIcon onClick={() => props.onAddContainerClick(containerState.id)}/>
            </Tooltip>
            </Grid>
        </Grid>
    );
}