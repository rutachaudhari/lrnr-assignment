import React, { useState } from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Radio from '@material-ui/core/Radio';

export default function CustomDialog(props) {
    
    const [selectedValue] = useState('container');
    const [open, setOpen] = useState(props);
    console.log(open)
    const handleClose = () => {
        setOpen(false);
    };

    const handleDialogResponse = (event) => {
        setOpen(false);
        props.handleDialogResponse(event)
    };

    return (
        <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
            <DialogTitle id="form-dialog-title">Add container or node?</DialogTitle>
            <DialogContent>
                Container <Radio
                    checked={selectedValue === 'container'}
                    onChange={handleDialogResponse}
                    value="container"
                    name="radio-button-demo"
                    inputProps={{ 'aria-label': 'container' }}
                />
                Node <Radio
                    checked={selectedValue === 'node'}
                    onChange={handleDialogResponse}
                    value="node"
                    name="radio-button-demo"
                    inputProps={{ 'aria-label': 'node' }}
                />
            </DialogContent>
        </Dialog>
    );
}
