import React from 'react';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import AddIcon from '@material-ui/icons/Add';
import Tooltip from '@material-ui/core/Tooltip';

export default function AddContainerPane(props) {
    return (
        <Paper>
            <Grid
                container
                direction="row"
                justify="flex-start">
                <Grid item xs>
                    DFIN
                </Grid>

                <Grid item xs>
                <Tooltip title="Create Container" arrow placement="top"> 
                <AddIcon onClick={props.onButtonClick}/>
                </Tooltip>
                </Grid>
            </Grid>

        </Paper>
    );
}