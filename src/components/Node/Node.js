import React , { useState } from 'react';
import TreeItem from '@material-ui/lab/TreeItem';

export default function Node(props) {
    const [node] = useState(props);
    return(
        <TreeItem
        nodeId={node.id}
        label={node.label}>

        </TreeItem>
    );
}